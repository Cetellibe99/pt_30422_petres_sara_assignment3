package Presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.util.Scanner;

/** @author Petres Sara
 *
 * This class generates the InputSet object, which has a string as constructor which should be the input file's name
 * the readCommands method reads the input data and does the parseing i.e.
 * removes the : and , characters for the respecting command type
 *
 * Then we add a command to a list of command objects which contains a command name like insert, order, product or delete
 * Also, a tablename: like client, product, order
 * and the arguments, like apple,20,1 etc.
 *
 * */

import Model.Command;

public class InputSet {
    public List <Command> commandList;

    public InputSet(){
        commandList = new ArrayList<>();
    }

    public void readCommands(String inputFile){

        File file = new File(inputFile);
        Scanner sc=null;

        try {
             sc = new Scanner(file);
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

        int i=0;
        Command command=null;
        String nextline;

        while (sc.hasNext()){

        nextline = sc.nextLine();

        String[] arr = nextline.split(" ",5);

        //CASE I: command is INSERT

            /** There are 2 types of insert commands defined:
             *
             * examples:
             * Insert client: Ion Popescu, Bucuresti
             * Insert product: peach
             * */

            if (arr[0].equals("Insert")){
                if(arr[1].equals("client:")) {

                    arr[1] = removeLastChar(arr[1]); //remove ':'
                    arr[3] = removeLastChar(arr[3]); //remove ','

                    command = new Command(arr[0], arr[1]);
                    command.argumentList.add(arr[2]);
                    command.argumentList.add(arr[3]);
                    command.argumentList.add(arr[4]);
                    // command.printCommand();
                    this.commandList.add(command);
                }
               else  {

                    arr[1] = removeLastChar(arr[1]); //remove ':'
                    arr[2] = removeLastChar(arr[2]);
                    arr[3] = removeLastChar(arr[3]); //remove ','

                    command = new Command(arr[0], arr[1]);
                    command.argumentList.add(arr[2]);
                    command.argumentList.add(arr[3]);
                    command.argumentList.add(arr[4]);
                    // command.printCommand();
                    this.commandList.add(command);

                }
            }
            //CASE II: command is  REPORT

            /** We don't have to remove any character at this command
             * since it has a structure like
             * Report product
             * (without any commas etc. )
             * */


            else if (arr[0].equals("Report")){
                command = new Command(arr[0],arr[1]);
                this.commandList.add(command); //argumentlist is empty
            }
            //CASE III: command is DELETE

            /** Two types of command here, we remove the , and : characters according to the command type*/


            else if (arr[0].equals("Delete")){
                arr[1] = removeLastChar(arr[1]);
                if (arr.length>3){
                    arr[3] = removeLastChar(arr[3]);
                }
                command = new Command(arr[0],arr[1]);

                int k=2;
                while (k<arr.length){
                    command.argumentList.add(arr[k]);
                    ++k;

                }

                this.commandList.add(command);
            }

            /** In case of the order command, I dont specify a tablename, simply just set it to null*/



            //CASE IV: command is ORDER
            else if(arr[0].equals("Order:")){
                arr[0] = removeLastChar(arr[0]);
                arr[2] = removeLastChar(arr[2]);
                arr[3] = removeLastChar(arr[3]);
                command = new Command(arr[0],null);
                command.argumentList.add(arr[1]);
                command.argumentList.add(arr[2]);
                command.argumentList.add(arr[3]);
                command.argumentList.add(arr[4]);
                this.commandList.add(command);

            }
            else{
                System.out.println("SOME READING ERROR");
            }


        }

        sc.close();

    }

    public static String removeLastChar(String str){
        return str.substring(0, str.length()-1);
    }

    public void printCommandList(){
        for (Command com : this.commandList){
            com.printCommand();
        }
    }

}
