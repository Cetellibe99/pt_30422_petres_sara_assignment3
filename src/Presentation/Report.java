package Presentation;

import DataAccess.DBConnect;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;


/**
 * @author Petres Sara
 *
 * This is the PDF generator class
 *
 * It generates three types of pdf:
 * I. a report type by createPDF()
 * II. a bill type by generateBill()
 * III. an under-stock message type by generateUnderStockMessage()
 *
 *
 *
 *
 *
 * */


public class Report {
    String tableName;

    public Report(String tableName){
        this.tableName = tableName;
    }

    public void createPDF() throws SQLException {

        //create connection
        DBConnect conn = new DBConnect();
        Connection myConnection = conn.createConn();

        Statement query = myConnection.createStatement();
        ResultSet rs = query.executeQuery("SELECT* FROM warehouse."+this.tableName);


        List <String>columnlist = new ArrayList<>();
        if (this.tableName.equals("client")){
            columnlist.add("clientID"); columnlist.add("firstName"); columnlist.add("lastName"); columnlist.add("city");
        }
        else if(this.tableName.equals("product")){
            columnlist.add("productID"); columnlist.add("productName"); columnlist.add("quantity"); columnlist.add("price");
        }
        else if (this.tableName.equals("order")){
            columnlist.add("orderID"); columnlist.add("firstName"); columnlist.add("lastName"); columnlist.add("productName"); columnlist.add("quantity");
        }

        Document document = new Document();
        try{
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Report "+tableName+".pdf"));
            document.open();
            document.add(new Paragraph("Report " +tableName+ "\n"));

            PdfPTable table = new PdfPTable(columnlist.size());
            table.setWidthPercentage(100);
            table.setSpacingBefore(10f);
            table.setSpacingAfter(10f);

            PdfPCell cells[] = new PdfPCell[columnlist.size()];
            for (int i=0; i<columnlist.size();i++){
                cells[i] = new PdfPCell(new Paragraph(columnlist.get(i)));

                cells[i].setBorderColor(BaseColor.LIGHT_GRAY);
                cells[i].setPaddingLeft(10);
                cells[i].setHorizontalAlignment(Element.ALIGN_CENTER);
                cells[i].setVerticalAlignment(Element.ALIGN_MIDDLE);

            }

            for (int i=0; i<columnlist.size(); ++i){
                table.addCell(cells[i]);
            }

            PdfPCell cella;


            while (rs.next()) {

               for (int j=0; j<columnlist.size(); j++)
               {
                   cella = new PdfPCell(new Paragraph(rs.getString(columnlist.get(j))));
                   cella.setBorderColor(BaseColor.LIGHT_GRAY);
                   cella.setPaddingLeft(10);
                   cella.setHorizontalAlignment(Element.ALIGN_CENTER);
                   cella.setVerticalAlignment(Element.ALIGN_MIDDLE);
                   table.addCell(cella);
               }

            }

            document.add(table);
            document.close();
            writer.close();
        }
        catch(DocumentException e){
            e.printStackTrace();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

        myConnection.close();

    }

   /* Generate a bill in
    pdf format with the order and total
    price of 5*/
    public static void generateBill(String order, String totalPrice) throws Exception{
       // System.out.println("I generate bill");

        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Bill.pdf"));
            document.open();
            document.add(new Paragraph(order));
            document.add(new Paragraph(totalPrice));
            document.close();
            writer.close();

        }
        catch(DocumentException e){
            e.printStackTrace();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

    }

    public static void generateUnderStockMessage() throws DocumentException {
        //System.out.println("I generate under stock message");

        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("UnderStockMessage.pdf"));
            document.open();
            document.add(new Paragraph("Sorry, but this is an UNDER STOCK message :( "));
            document.close();
            writer.close();

        }
        catch(DocumentException e){
            e.printStackTrace();
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }

    }

}
