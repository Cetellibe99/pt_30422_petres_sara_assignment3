package Presentation;


import BusinessLogic.DAO;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import Model.Client;
import Model.Command;
import com.itextpdf.text.DocumentException;


/** @author Petres Sara 30422
 * @version 1.0
 * @since April 2020
 *
 * @title Order management using MySQL
 *
 *
 * SHORT DESCRIPTION
 *
 * A set of commands is read in Main using the InputSet class
 * After that the DAO class' operation method is invoked for each command object
 * (A command object contains the name of the operation, the table and the column names)
 * Parallely an accountancy() method is invoked after the operation.
 * It is responsible for recording each command to the command extra table
 *
 * The DAO.operation method invokes some methods of the GenerateSQLStatement class which proceeds the command,
 * decides which further methods should be invoked. Then the operations (insert, delete, edit: by delete+insert) are performed
 * by the inserObject() and deleteObject() generic methods which are using reflection tools
 * in order to improve performance
 *
 * The connection to the database is estabilished in the DBConnect class.
 *
 * The required reports: bill, under-stock message and report client/order/product are generated as pdf files
 * using the Report class
 *
 * */

public class Main {

    public static void main(String[] args) throws Exception {

        String filename = args[0];
        InputSet inputSet = new InputSet();
        inputSet.readCommands(filename);
        //inputSet.readCommands("input.txt");
       // inputSet.printCommandList();

        DAO<Client> simulation = new DAO<>();

        //simulation.operation(inputSet.commandList.get(20));



       for (int i=0; i<inputSet.commandList.size();i++){

            simulation.operation(inputSet.commandList.get(i));
           accountancy(inputSet.commandList.get(i)); //recording each command in the command table
           
        }


    }

    public static void accountancy (Command command) throws Exception{

        /** Records the command - which is the unit of any action in this program - to the command table*/

        DAO.insertObject(command);
    }

}
