package DataAccess;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/** @author Petres Sara
 *
 * This class estabilish the correct database connection ot MYSQL server
 *
 * */

public class DBConnect {

        public Connection createConn(){

            Connection connect = null;
            try{
                Class.forName("com.mysql.cj.jdbc.Driver");
                 connect = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/warehouse",  "root",  "Petres-Sara99");

            }
            catch(ClassNotFoundException ex){
                Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE,  null,ex);

            }
            catch(SQLException ex){
                Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null,ex);
                ex.printStackTrace();
            }
            return connect;
        }
}
