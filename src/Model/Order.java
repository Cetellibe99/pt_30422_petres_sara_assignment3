package Model;

/** @author Petres Sara
 *
 * Each class of the Model package
 * contains a generateInsertCommand() and a generateDeleteCommand()
 * which
 * @return a string containing the sql which deletes the object
 * this is used to simplified separating at each deletion, insertion the cases
 * of creating the sql statement
 *
 * Simply, each object "knows" how to delete or insert itself
 *
 * */

public class Order {
    String firstName;
    String lastName;
    String productName;
    Double quantity;

    public Order(String firstName, String lastName, String productName, Double quantity) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.productName = productName;
        this.quantity = quantity;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String generateInsertCommand(){
        String sql;
        String q = String.valueOf(this.quantity);
        sql = "INSERT INTO warehouse.order (firstName, lastName, productName, quantity) VALUES ('"+this.firstName+"', '"+this.lastName+"', '"+this.productName+"', '"+q+"')";
       //  sql = "INSERT INTO order (firstName, lastName, productName, quantity) VALUES ('"+this.firstName+"', '"+this.lastName+"', '"+this.productName+"', '5')";

      //  System.out.println("sql is : "+sql);
        return sql;

    }
    public String generateDeleteCommand(){
        String sql = "DELETE FROM warehouse.order WHERE productName = '"+this.productName+"' LIMIT 1";
        return sql;

    }
}
