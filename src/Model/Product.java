package Model;

/** @author Petres Sara
 *
 * Each class of the Model package
 * contains a generateInsertCommand() and a generateDeleteCommand()
 * which
 * @return a string containing the sql which deletes the object
 * this is used to simplified separating at each deletion, insertion the cases
 * of creating the sql statement
 *
 * Simply, each object "knows" how to delete or insert itself
 *
 * */

public class Product {
   public String productName;
   public  Double quantity;
   public  Double price;

    public Product(String productName, Double quantity, Double price) {
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    public void printProduct(){
        System.out.println("product data : " + this.productName+ " "+this.quantity+" "+this.price);
    }
    public String generateDeleteCommand(){
        String sql = "DELETE FROM product WHERE productName = '"+this.productName+"' LIMIT 1";
        return sql;

    }
    public String generateInsertCommand(){
        String sql = "INSERT INTO product (productName, quantity, price) VALUES ('"+this.productName+"', '"+this.quantity+"', '"+this.price+"')";
        return sql;
    }
}
