package Model;



/** @author Petres Sara
 *
 * Each class of the Model package
 * contains a generateInsertCommand() and a generateDeleteCommand()
 * which
 * @return a string containing the sql which deletes the object
 * this is used to simplified separating at each deletion, insertion the cases
 * of creating the sql statement
 *
 * Simply, each object "knows" how to delete or insert itself
 *
 * */




public class Client {
    String firstName;
    String lastName;
    String city;

    public Client(String firstName, String lastName, String city){
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String generateInsertCommand(){
        String sql = "INSERT INTO client (firstName, lastName, city) VALUES ('"+this.firstName+"', '"+this.lastName+"', '"+this.city+"')";

        return sql;
    }

    public String generateDeleteCommand(){
        String sql = "DELETE FROM client WHERE firstName = '"+this.firstName+"' AND lastName = '"+this.lastName+"' LIMIT 1";
        return sql;

    }
}

