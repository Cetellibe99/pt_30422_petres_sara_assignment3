package Model;

/** @author Petres Sara
 *
 * Each class of the Model package
 * contains a generateInsertCommand() and a generateDeleteCommand()
 * which
 * @return a string containing the sql which deletes the object
 * this is used to simplified separating at each deletion, insertion the cases
 * of creating the sql statement
 *
 * Simply, each object "knows" how to delete or insert itself
 *
 * */

import java.util.ArrayList;
import java.util.List;

public class Command {
    public String operationName;
    public String tableName;
    public List <String> argumentList;

    public Command(String operationName, String tableName) {
        this.tableName = tableName;
        this.operationName = operationName;
        argumentList = new ArrayList<>();
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getArgumentList() {
        return argumentList;
    }

    public void setArgumentList(List<String> argumentList) {
        this.argumentList = argumentList;
    }
    public void printCommand(){
        System.out.print(" "+this.operationName+"  "+this.tableName + " ");
        for (String s: this.argumentList){
            System.out.print("  "+s);
        }
        System.out.println(" ");

    }

    public String generateInsertCommand(){
        String sql = null;
        if (this.argumentList.size()==0){ //REPORT
            String operation = this.operationName + " "+this.tableName;
             sql = "INSERT INTO command(operation, argument1, argument2, argument3, argument4) VALUES ('"+operation+"', '-', '-', '-', '-')";

        }
        else if(this.argumentList.size()==1){
            String operation = this.operationName +" "+ this.tableName;
            sql = "INSERT INTO command (operation, argument1, argument2, argument3, argument4) VALUES ('" + operation + "', '" + this.argumentList.get(0) + "', '-', '-', '-')";

        }
        else if (this.argumentList.size()==2) //INSERT or DELETE
        {
            String operation = this.operationName +" "+ this.tableName;
            sql = "INSERT INTO command (operation, argument1, argument2, argument3, argument4) VALUES ('" + operation + "', '" + this.argumentList.get(0) + "', '" + this.argumentList.get(1) + "', '-', '-')";
        }
        else if (this.argumentList.size()==3) //INSERT or DELETE
        {
            String operation = this.operationName +" "+ this.tableName;
             sql = "INSERT INTO command (operation, argument1, argument2, argument3, argument4) VALUES ('" + operation + "', '" + this.argumentList.get(0) + "', '" + this.argumentList.get(1) + "', '" + this.argumentList.get(2) + "', '-')";
        }
        else if (this.argumentList.size()==4){ //ORDER
            sql = "INSERT INTO command (operation, argument1, argument2, argument3, argument4) VALUES ('" + this.operationName + "', '" + this.argumentList.get(0) + "', '" + this.argumentList.get(1) + "', '" + this.argumentList.get(2) + "', '"+this.argumentList.get(3)+"')";

        }
        return sql;
    }


}
