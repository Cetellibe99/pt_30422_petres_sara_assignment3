package BusinessLogic;

import DataAccess.DBConnect;

import java.sql.Connection;
import java.sql.*;
import Model.Command;
import Model.Product;
import Presentation.Report;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

/** @author Petres Sara
 *
 *
 * this class contains the executeOrder() and the generic insertion and deletion methods
 *
 * these are the main responsible for handling insert, delete or order command
 *
 *
 *
 * */

public class DAO<T> {

    public  void operation( Command command) throws Exception{

        GenerateSQLStatement toExecute = new GenerateSQLStatement();
        toExecute.sqlStatement(command);

    }

    /**
     * The executeOrder command
     *
     * I. finds the ordered product
     * -if the product is on stock (the ordered products quantity is less then the quantity of the found
     * productname's quantity from the database)
     * => a bill pdf is created
     * => a new product object is created with decremented quantity
     * => the old product is deleted from the table
     * => and the new product is inserted (edition)
     *
     *
     * */


    public static void executeOrder(Command command) throws Exception {

        String productName = command.argumentList.get(2);
        String quantity = command.argumentList.get(3);
        //update stock
        //create bill

        DBConnect conn = new DBConnect();
        Connection myConnection = conn.createConn();

        String sql = "SELECT productName, quantity, price FROM product WHERE productName = '"+productName+ "'";
        //  System.out.println("sql is: " +sql);
        Statement query = myConnection.createStatement();

        ResultSet rs = query.executeQuery(sql);

        String pn=null, qu=null,pr=null;
        Double quanti=0d;
        Double pri = 0d;


        if(rs.next()){

            pn = rs.getString("productName");
            qu = rs.getString("quantity");
            pr = rs.getString("price");

        }
        quanti = Double.parseDouble(qu);
        pri = Double.parseDouble(pr);

        Product product = new Product(pn,quanti,pri);

        Double billQuantity = Double.parseDouble(quantity);
        Double totalPri = billQuantity*pri;

        Double temp = product.quantity;
        temp = temp - billQuantity;

        if (temp>=0){
            deleteObject(product);
            product.quantity = temp; //update quantity

            String order = command.operationName+": "+command.argumentList.get(0)+
                    " "+command.argumentList.get(1)+", "+command.argumentList.get(2)+", "+command.argumentList.get(3);

            String totalPrice= "Total price is : " +quantity+"X"+pr+"= "+String.valueOf(totalPri);

            GenerateSQLStatement.insert(command);
            Report.generateBill(order,totalPrice);
            insertObject(product);

        }
        else{
            Report.generateUnderStockMessage();
        }

        myConnection.close();

    }

    /** The deleteObject() and the insertObject() methods
     *
     *these are parameterized methods, using a
     * @param t object which can be client, command, order or product type
     *
     * The methods are using the tools of Java reflection
     *          The methods Method[] vector finds the corresponding generateDeleteCommand or generateInsertCommand of
     *          each object and invokes it
     *
     *          It couldn't be implemented without reflection since the system doesn't know about the t object if it has these methods
     *
     *
     *
     *
     * */

    public static <T> void deleteObject(T t) throws SQLException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        //connection
        //get command
        //execute
        //close connection
        DBConnect conn = new DBConnect();
        Connection myConnection = conn.createConn();

        Class clasa = t.getClass();
        Method[] methods = clasa.getMethods();

        Method getSQL = clasa.getDeclaredMethod("generateDeleteCommand");
        String sql = (String) getSQL.invoke(t);

        Statement query = myConnection.createStatement();
        query.executeUpdate(sql);

        myConnection.close();

    }
    public static <T> void insertObject(T t) throws Exception{

        DBConnect conn = new DBConnect();
        Connection myConnection = conn.createConn();
        Class clasa = t.getClass();
        Method[] methods = clasa.getMethods();
        Method getSQL = clasa.getDeclaredMethod("generateInsertCommand");

        String sql = (String) getSQL.invoke(t);

        Statement query = myConnection.createStatement();
        query.executeUpdate(sql);

        myConnection.close();

    }


}

