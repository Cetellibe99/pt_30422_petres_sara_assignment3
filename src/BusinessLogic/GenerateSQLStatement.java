package BusinessLogic;

import Model.Client;
import Model.Command;
import Model.Order;
import Model.Product;
import Presentation.Report;

/**@author Petres Sara
 *
 * The sqlStatement() method is a sharepoint method i.e. the DAO class sends to this method the command. The
 * sqlStatement() methods decides if the nature of the command(insert, delete, report, order)
 * for delete and insert the delete() and insert() functions of this class creates an
 * object of the corresponding type and sends to DAO classes generic insertObject or deleteObject method
 * which inserts or deletes the object T
 *
 * for orders the process is more complex, the executeOrder() method will handle this situation from
 * the DAO class
 *
 * for reports the methods of the Report class are invoked
 *
 *
 *
 *
 * */


public class GenerateSQLStatement {

    public static void insert(Command command) throws Exception{

        if (command.tableName == null) { //order

            Double quantity = Double.parseDouble(command.argumentList.get(3));
            Order order = new Order(command.argumentList.get(0),command.argumentList.get(1), command.argumentList.get(2),quantity);

            DAO.insertObject(order);

        }
       else if (command.tableName.equals( "client") ){

            Client client = new Client(command.argumentList.get(0),command.argumentList.get(1),command.argumentList.get(2));
            DAO.insertObject(client);
        }
        else if (command.tableName.equals("product")){

            Double quantity = Double.parseDouble(command.argumentList.get(1));
            Double price = Double.parseDouble(command.argumentList.get(2));

            Product product = new Product(command.argumentList.get(0), quantity, price);
            DAO.insertObject(product);
        }
    }

    public void delete (Command command) throws Exception{

      if(command.tableName == null ) //order
      {
          Double quantity = Double.parseDouble(command.argumentList.get(3));
          Order order = new Order(command.argumentList.get(0),command.argumentList.get(1), command.argumentList.get(2),quantity);

          DAO.deleteObject(order);
      }
      else if (command.tableName.equals("client")){
          Client client = new Client(command.argumentList.get(0),command.argumentList.get(1),command.argumentList.get(2));
          DAO.deleteObject(client);

      }
      else if (command.tableName.equals("product")){

          Product product = new Product(command.argumentList.get(0), null, null);
          DAO.deleteObject(product);

      }

       // return statement;
    }

    public void sqlStatement(Command command) throws Exception {
        //String statement = null;


        if (command.operationName.equals("Insert")){

            insert(command);
        }
        else if(command.operationName.equals("Delete")){

            delete(command);
        }
        else if (command.operationName.equals("Report")){

            Report report = new Report(command.tableName);
            report.createPDF();

       }
        else if (command.operationName.equals("Order")){

          //  insert(command);
            DAO.executeOrder(command);

        }

    }
}
